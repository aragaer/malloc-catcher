#include <stdlib.h>
#include <stdio.h>

#define MB (1024*1024)

int main() {
  for (size_t i = 0; i < 100; i++) {
    printf("Allocating %zu -> %zu\n", i*MB, (i+1)*MB);
    malloc(1*MB);
  }
}
