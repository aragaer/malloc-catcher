#define _GNU_SOURCE
#include <dlfcn.h>
#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unwind.h>

#include <unistd.h>

#include <jsw_rbtree.h>

#include "mc.h"

void *(*real_malloc)(size_t) = NULL;
void (*real_free)(void *) = NULL;
void *(*real_memalign)(size_t, size_t) = NULL;
void *(*real_calloc)(size_t, size_t) = NULL;
void *(*real_realloc)(void *, size_t) = NULL;

void *_real_malloc(size_t size) {
  return real_malloc(size);
}

void _real_free(void *ptr) {
  real_free(ptr);
}

#ifdef PTHREAD_RECURSIVE_MUTEX_INITIALIZER_N
static pthread_mutex_t mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_N;
#else
static pthread_mutex_t mutex;
#endif
static int malloc_hook_active = 0;
static volatile size_t total_allocated = 0;
static size_t allocation_threshold = 0;

struct alloc_info {
  void *addr;
  void *caller;
  size_t size;
  size_t real_size;
};

static int alloc_info_cmp(const void *p1, const void *p2) {
  struct alloc_info *i1 = (typeof(i1)) p1;
  struct alloc_info *i2 = (typeof(i2)) p2;
  if (i1->addr == i2->addr)
    return 0;
  if (i1->addr > i2->addr)
    return 1;
  return -1;
}

static void *alloc_info_dup(void *p) {
  struct alloc_info *orig = (typeof(orig)) p;
  struct alloc_info *result = real_malloc(sizeof(*result));
  if (result == NULL)
    return NULL;
  memcpy(result, orig, sizeof(*result));
  return result;
}

static void alloc_info_rel(void *p) {
  real_free(p);
}

jsw_rbtree_t *alloc_data;

#ifndef PTHREAD_RECURSIVE_MUTEX_INITIALIZER_N
__attribute__((constructor)) void mutex_init(void) {
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(&mutex, &attr);
  pthread_mutexattr_destroy(&attr);
}
#endif

#define MB (1024*1024)

static int is_in_init;

static void init(void) {
  if (is_in_init)
    return;
  is_in_init = 1;
  real_malloc = dlsym(RTLD_NEXT, "malloc");
  #ifndef __GLIBC__
  real_calloc = dlsym(RTLD_NEXT, "calloc");
  #endif
  real_realloc = dlsym(RTLD_NEXT, "realloc");
  real_free = dlsym(RTLD_NEXT, "free");
  real_memalign = dlsym(RTLD_NEXT, "memalign");
  printf("Got functions: malloc = %p, free = %p\n",
         real_malloc, real_free);
  alloc_data = jsw_rbnew(alloc_info_cmp,
                         alloc_info_dup,
                         alloc_info_rel);
  char *threshold = getenv("MC_THRESHOLD");
  if (threshold != NULL)
    allocation_threshold = strtoul(threshold, NULL, 10) * MB;
  malloc_hook_active = 1;
  is_in_init = 0;
}

static _Unwind_Reason_Code handler(struct _Unwind_Context* context, void* ref) {
  intptr_t func_start = _Unwind_GetRegionStart(context);
  intptr_t ret = _Unwind_GetIP(context);
  if (ret)
    printf("0x%08" PRIxPTR
           " (0x%08" PRIxPTR
           " +0x%" PRIxPTR ")\n",
      ret, func_start, ret - func_start);
  return _URC_NO_REASON;
}

static inline void do_backtrace() {
  write(1, "Backtrace:\n", 11);
  _Unwind_Backtrace(&handler, NULL);
}

__attribute__((noreturn)) static void dump_allocation_info_and_exit(void *cause) {
  printf("Memory threshold reached\n");
  printf("Allocations:\n");
  jsw_rbtrav_t *trav = jsw_rbtnew();
  for (void *ptr = jsw_rbtfirst(trav, alloc_data);
       ptr;
       ptr = jsw_rbtnext(trav)) {
    struct alloc_info *info = (typeof(info)) ptr;
    printf("  %p - %zu by %p", info->addr, info->real_size, info->caller);
    if (info->addr == cause)
      printf(" <-");
    printf("\n");
  }
  fflush(stdout);
  jsw_rbtdelete(trav);
  char buf[4096];
  int fd = open("/proc/self/maps", O_RDONLY);
  if (fd == -1)
    perror("Can't open /proc/self/maps");
  else {
    ssize_t res;
    write(1, "Maps:\n", 6);
    fflush(stdout);
    while ((res = read(fd, buf, sizeof(buf))))
      write(1, buf, res);
    close(fd);
  }
  do_backtrace();
  fflush(stdout);
  write(1, "End:\n", 5);
  exit(-1);
}

static inline void malloc_hook(size_t size, void *addr, void *caller) {
  if (addr == NULL)
    return;
  size_t real_size = size;
  if (size < 128)
    size = 128;
  total_allocated += size;
  struct alloc_info info = {addr, caller, size, real_size};
  jsw_rbinsert(alloc_data, &info);
  if (allocation_threshold && total_allocated > allocation_threshold)
    dump_allocation_info_and_exit(addr);
}

static inline void free_hook(void *addr, void *caller) {
  if (addr == NULL)
    return;
  struct alloc_info info = {addr, caller, 0};
  struct alloc_info *found = jsw_rbfind(alloc_data, &info);
  if (found != NULL) {
    total_allocated -= found->size;
    jsw_rberase(alloc_data, found);
  }
}

static inline void realloc_hook(size_t size, void *old, void *addr, void *caller) {
  if (addr == NULL)
    return;
  free_hook(old, caller);
  malloc_hook(size, addr, caller);
}

void *malloc(size_t size) {
  pthread_mutex_lock(&mutex);
  if (real_malloc == NULL)
    init();
  void *result;
  if (malloc_hook_active) {
    malloc_hook_active = 0;
    result = real_malloc(size);
    malloc_hook(size, result, __builtin_return_address(0));
    malloc_hook_active = 1;
  } else
    result = real_malloc(size);
  pthread_mutex_unlock(&mutex);
  return result;
}

#ifndef __GLIBC__
void *calloc(size_t nmemb, size_t size) {
  pthread_mutex_lock(&mutex);
  if (real_malloc == NULL)
    init();
  void *result;
  if (malloc_hook_active) {
    malloc_hook_active = 0;
    result = real_calloc(nmemb, size);
    malloc_hook(nmemb*size, result, __builtin_return_address(0));
    malloc_hook_active = 1;
  } else
    result = real_calloc(nmemb, size);
  pthread_mutex_unlock(&mutex);
  return result;
}
#endif

void *realloc(void *ptr, size_t size) {
  pthread_mutex_lock(&mutex);
  if (real_malloc == NULL)
    init();
  void *result;
  if (malloc_hook_active) {
    malloc_hook_active = 0;
    result = real_realloc(ptr, size);
    realloc_hook(size, ptr, result, __builtin_return_address(0));
    malloc_hook_active = 1;
  } else
    result = real_realloc(ptr, size);
  pthread_mutex_unlock(&mutex);
  return result;
}

void *memalign(size_t alignment, size_t size) {
  pthread_mutex_lock(&mutex);
  if (real_malloc == NULL)
    init();
  void *result;
  if (malloc_hook_active) {
    malloc_hook_active = 0;
    result = real_memalign(alignment, size);
    if (result == NULL)
      result = real_malloc(size);
    malloc_hook(size, result, __builtin_return_address(0));
    malloc_hook_active = 1;
  } else
    result = real_memalign(alignment, size);
  pthread_mutex_unlock(&mutex);
  return result;
}

void free(void *ptr) {
  pthread_mutex_lock(&mutex);
  if (real_free == NULL)
    init();
  if (malloc_hook_active) {
    malloc_hook_active = 0;
    free_hook(ptr, NULL);
    malloc_hook_active = 1;
  }
  real_free(ptr);
  pthread_mutex_unlock(&mutex);
}
