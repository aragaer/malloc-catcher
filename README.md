### Build

When cross-compiling using WRT toolchain specify STAGING_DIR.

### Run

MC_THRESHOLD variable determines an approximate size (in megabytes) of
allocation when the library stops the application and dumps the
allocation information.

Use LD_PRELOAD to run the library.

### Analyze

The analyze.py script can be used to process the library output into
more readable format.

### Known issues

libc implementation of dlsym uses calloc which leads to infinite
recursion during library initialization.
