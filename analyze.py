#!/usr/bin/env python3
import sys

def parse_map(line):
    addrs, _, _, _, _, *where = line.split()
    start, end = addrs.split('-')
    return int(start, 16), int(end, 16), (where + [''])[0]

def parse_trace(trace):
    addr, _ = trace.split(maxsplit=1)
    return int(addr, 16)

def main():
    allocations = []
    maps = []
    backtrace = []
    destination = None
    for line in sys.stdin.readlines():
        if 'Allocations:' in line:
            destination = allocations
            continue
        elif 'Maps:' in line:
            destination = maps
            continue
        elif 'Backtrace:' in line:
            destination = backtrace
            continue
        elif 'End:' in line:
            break
        if destination is None:
            continue
        destination.append(line.strip())
    maps = [parse_map(m) for m in maps]
    for trace in backtrace:
        addr = parse_trace(trace)
        for (start, end, name) in maps:
            if start <= addr <= end:
                print(hex(addr), name, hex(addr-start))

if __name__ == '__main__':
    main()
