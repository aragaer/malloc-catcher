#ifndef MC_H
#define MC_H

#include <stdlib.h>

extern void *(*real_malloc)(size_t);
extern void (*real_free)(void *);

#endif  // MC_H
