lib = libmc.so
test_app = test_alloc

CFLAGS += -Wall -Werror -fPIC -funwind-tables

CPPFLAGS += -Ired-black-tree/src
VPATH = red-black-tree/src

ifneq ($(STAGING_DIR),)
  CC = $(wildcard $(STAGING_DIR)/toolchain*/bin/*muslgnueabi-gcc)
endif

all: $(lib)

$(lib): mc.o jsw_rbtree.o
	$(CC) -shared -o $@ $^ -ldl -lpthread -O3

jsw_rbtree.o: CPPFLAGS += -Dmalloc=_real_malloc -Dfree=_real_free

$(test_app): CFLAGS+=-Wno-unused-result

test: $(lib) $(test_app)
	-env MC_THRESHOLD=10 LD_PRELOAD=./$(lib) ./$(test_app)

.PHONY: all clean test

clean:
	rm -f $(lib) *.o $(test_app)
